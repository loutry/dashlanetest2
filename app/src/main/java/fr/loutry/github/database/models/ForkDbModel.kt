package fr.loutry.github.database.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "forks",
    indices = [Index(value = ["creationDate"])]
)
data class ForkDbModel(
    @PrimaryKey
    val id: String,
    val title: String,
    val description: String,
    val creationDate: String,
    val userLogin: String,
    val userAvatar: String
)
