package fr.loutry.github.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.loutry.github.database.models.ForkDbModel

@Dao
interface ForkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertForks(forks: List<ForkDbModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFork(fork: ForkDbModel)

    @Delete
    suspend fun deleteFork(fork: ForkDbModel)

    @Query("SELECT * FROM forks WHERE id = :id")
    suspend fun getFork(id: String): ForkDbModel?

    @Query("SELECT * FROM forks ORDER BY creationDate DESC")
    fun getPagedForksFactory(): DataSource.Factory<Int, ForkDbModel>

    @Query("SELECT COUNT(id) FROM forks")
    suspend fun getCount(): Int

    @Query("SELECT * FROM forks ORDER BY creationDate DESC LIMIT 1")
    suspend fun getMostRecentFork(): ForkDbModel?
}