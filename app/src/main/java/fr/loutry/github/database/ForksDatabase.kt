package fr.loutry.github.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.loutry.github.database.models.ForkDbModel

@Database(
    entities = [ForkDbModel::class],
    version = 1
)
abstract class ForksDatabase : RoomDatabase() {

    abstract fun getDao(): ForkDao

    companion object {

        private const val DATABASE_NAME = "Forks.db"

        @Volatile
        var instance: ForksDatabase? = null

        fun getInstance(context: Context): ForksDatabase {
            if (instance == null) {
                synchronized(ForksDatabase::class) {
                    instance = buildDatabase(context)
                }
            }
            return instance!!
        }

        private fun buildDatabase(context: Context): ForksDatabase {
            return Room
                .databaseBuilder(
                    context.applicationContext,
                    ForksDatabase::class.java,
                    DATABASE_NAME
                )
                .build()
        }
    }
}
