package fr.loutry.github.repository.usecases

import android.util.Log
import fr.loutry.github.api.ApiDefault
import fr.loutry.github.api.GitHubService
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.database.models.ForkDbModel
import fr.loutry.github.repository.mappers.toDbModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FetchApiAndStoreInDbUseCase(
    private val api: GitHubService,
    private val database: ForksDatabase
) {

    suspend operator fun invoke(page: Int): List<ForkDbModel> {
        try {
            val data = api.listForks(
                ApiDefault.OWNER,
                ApiDefault.REPOSITORY,
                page,
                ApiDefault.PAGE_SIZE
            ).map { apiModel ->
                apiModel.toDbModel()
            }

            withContext(Dispatchers.Default) {
                database.getDao().insertForks(data)
            }

            return data
        } catch (exception: Exception) {
            Log.e(Tag, "something bad happened :s", exception)
            return emptyList()
        }
    }

    companion object {
        private val Tag = FetchApiAndStoreInDbUseCase::class.java.simpleName
    }
}