package fr.loutry.github.repository.mappers

import fr.loutry.github.api.models.ForkApiModel
import fr.loutry.github.database.models.ForkDbModel

fun ForkApiModel.toDbModel(): ForkDbModel {
    return ForkDbModel(
        id = id.toString(),
        title = name,
        description = description,
        creationDate = created_at,
        userLogin = owner.login,
        userAvatar = owner.avatar_url
    )
}