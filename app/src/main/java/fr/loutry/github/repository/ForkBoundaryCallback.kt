package fr.loutry.github.repository

import androidx.paging.PagedList
import fr.loutry.github.api.ApiDefault
import fr.loutry.github.database.models.ForkDbModel
import fr.loutry.github.repository.usecases.FetchApiAndStoreInDbUseCase
import fr.loutry.github.repository.usecases.GetCurrentNetworkPageUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class ForkBoundaryCallback(
    private val coroutineScope: CoroutineScope,
    private val fetchApiAndStoreInDbUseCase: FetchApiAndStoreInDbUseCase,
    private val getCurrentNetworkPageUseCase: GetCurrentNetworkPageUseCase
) : PagedList.BoundaryCallback<ForkDbModel>() {

    private var isLoading = false

    override fun onZeroItemsLoaded() {
        if (isLoading) return
        isLoading = true
        coroutineScope.launch {
            fetchApiAndStoreInDbUseCase(page = ApiDefault.FIRST_PAGE)
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: ForkDbModel) {
        if (isLoading) return
        isLoading = true
        coroutineScope.launch {
            val currentPage = getCurrentNetworkPageUseCase()
            fetchApiAndStoreInDbUseCase(page = currentPage)
            fetchApiAndStoreInDbUseCase(page = currentPage + 1)
        }
    }
}