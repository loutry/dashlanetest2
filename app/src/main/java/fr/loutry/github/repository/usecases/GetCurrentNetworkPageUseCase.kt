package fr.loutry.github.repository.usecases

import fr.loutry.github.api.ApiDefault
import fr.loutry.github.database.ForksDatabase
import kotlin.math.ceil

class GetCurrentNetworkPageUseCase(private val database: ForksDatabase) {

    suspend operator fun invoke(): Int {
        // This can work only if we assume that our stored forks
        // hold all the most recent api forks
        // and no fork destruction occurred.
        val storedForksCount = database.getDao().getCount()
        return ceil(storedForksCount / ApiDefault.PAGE_SIZE.toDouble()).toInt()
    }
}