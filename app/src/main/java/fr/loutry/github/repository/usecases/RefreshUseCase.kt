package fr.loutry.github.repository.usecases

import fr.loutry.github.api.ApiDefault
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.database.models.ForkDbModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RefreshUseCase(
    private val database: ForksDatabase,
    private val fetchApiAndStoreInDbUseCase: FetchApiAndStoreInDbUseCase
) {

    suspend operator fun invoke() {
        withContext(Dispatchers.Default) {
            val mostRecentFork = database.getDao().getMostRecentFork()
            mostRecentFork?.let {
                refreshUntil(it)
            }
        }
    }

    /**
     * Fetch remote most recent data until we found our most recent stored item.
     */
    private suspend fun refreshUntil(fork: ForkDbModel) {
        var currentPage = ApiDefault.FIRST_PAGE
        var data : List<ForkDbModel>
        do {
            data = fetchApiAndStoreInDbUseCase(currentPage)
            currentPage++
        } while (data.any { item -> item.id == fork.id })
    }
}