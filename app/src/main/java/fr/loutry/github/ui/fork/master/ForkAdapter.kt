package fr.loutry.github.ui.fork.master

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fr.loutry.github.database.models.ForkDbModel
import fr.loutry.github.ui.fork.master.views.ForkView

class ForkAdapter(private val onItemClick: (forkId: String?) -> Unit) : PagedListAdapter<ForkDbModel, ForkViewHolder>(
    object : DiffUtil.ItemCallback<ForkDbModel>() {
        override fun areItemsTheSame(oldItem: ForkDbModel, newItem: ForkDbModel)
                = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ForkDbModel, newItem: ForkDbModel)
                = oldItem == newItem

    }
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForkViewHolder {
        val forkView = ForkView(parent.context)
        return ForkViewHolder(forkView)
    }

    override fun onBindViewHolder(holder: ForkViewHolder, position: Int) {
        val fork = getItem(position)
        holder.forkView.setImage(fork?.userAvatar)
        holder.forkView.setLabel(fork?.description)
        holder.forkView.setOnClickListener { onItemClick(fork?.id) }
    }
}

class ForkViewHolder(itemView: ForkView) : RecyclerView.ViewHolder(itemView) {
    val forkView = itemView
}
