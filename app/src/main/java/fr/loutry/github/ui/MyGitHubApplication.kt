package fr.loutry.github.ui

import android.app.Application
import com.facebook.stetho.Stetho
import fr.loutry.github.BuildConfig

class MyGitHubApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build()
            );
        }
    }
}