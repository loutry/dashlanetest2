package fr.loutry.github.ui.fork.master

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import fr.loutry.github.api.GitHubService
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.database.models.ForkDbModel
import fr.loutry.github.repository.ForkBoundaryCallback
import fr.loutry.github.repository.usecases.FetchApiAndStoreInDbUseCase
import fr.loutry.github.repository.usecases.GetCurrentNetworkPageUseCase
import fr.loutry.github.repository.usecases.RefreshUseCase
import kotlinx.coroutines.launch

class ForksViewModel(
    api: GitHubService,
    database: ForksDatabase
) : ViewModel() {

    private val fetchApiAndStoreInDbUseCase = FetchApiAndStoreInDbUseCase(api, database)
    private val refreshUseCase = RefreshUseCase(database, fetchApiAndStoreInDbUseCase)
    private val getCurrentNetworkPageUseCase = GetCurrentNetworkPageUseCase(database)

    val uiData: LiveData<PagedList<ForkDbModel>>

    init {
        viewModelScope.launch {
            refreshUseCase()
        }

        val factory = database.getDao().getPagedForksFactory()

        val config = PagedList.Config.Builder()
            .setPageSize(30)
            .setInitialLoadSizeHint(60)
            .setPrefetchDistance(10)
            .setEnablePlaceholders(false)
            .build()

        val boundaryCallback = ForkBoundaryCallback(
            coroutineScope = viewModelScope,
            fetchApiAndStoreInDbUseCase = fetchApiAndStoreInDbUseCase,
            getCurrentNetworkPageUseCase = getCurrentNetworkPageUseCase
        )

        uiData = LivePagedListBuilder(factory, config)
            .setBoundaryCallback(boundaryCallback)
            .build()
    }

}
