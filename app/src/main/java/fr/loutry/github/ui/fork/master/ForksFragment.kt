package fr.loutry.github.ui.fork.master

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.loutry.github.R
import fr.loutry.github.api.GitHubRepository
import fr.loutry.github.database.ForksDatabase

class ForksFragment : Fragment() {

    private val adapter = ForkAdapter { navigateTo(it) }

    private lateinit var viewModel: ForksViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.forks_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.main_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        context?.let { context ->
            initViewModel(context)
        }
    }

    private fun initViewModel(context: Context) {
        val factory = ForksViewModelFactory(
            api = GitHubRepository.getApi(resources),
            database = ForksDatabase.getInstance(context)
        )
        viewModel = ViewModelProviders.of(this, factory)
            .get(ForksViewModel::class.java)
        viewModel.uiData.observe(this, Observer { pagedList ->
            adapter.submitList(pagedList)
        })
    }

    private fun navigateTo(forkId: String?) {
        val bundle = bundleOf("forkId" to forkId)
        findNavController()
            .navigate(R.id.action_forksFragment_to_forkDetailsFragment, bundle)
    }
}
