package fr.loutry.github.ui.fork.master.views

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import fr.loutry.github.R

class ForkView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val imageView : ImageView
    private val labelView : TextView

    init {
        View.inflate(context, R.layout.forks_item, this)
        layoutParams = LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        gravity = Gravity.CENTER_VERTICAL
        orientation = HORIZONTAL
        isFocusable = true
        isClickable = true
        descendantFocusability = ViewGroup.FOCUS_BLOCK_DESCENDANTS

        val outValue = TypedValue()
        getContext().theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
        setBackgroundResource(outValue.resourceId)

        imageView = findViewById(R.id.fork_item_image)
        labelView = findViewById(R.id.fork_item_label)
    }

    fun setImage(imageUrl: String?) {
        when (imageUrl) {
            null -> imageView.setImageResource(R.drawable.shape_forks_avatar_placeholder)
            else -> {
                Picasso.get()
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.shape_forks_avatar_placeholder)
                    .into(imageView)
            }
        }
    }

    fun setLabel(label: String?) {
        labelView.text = label
    }
}
