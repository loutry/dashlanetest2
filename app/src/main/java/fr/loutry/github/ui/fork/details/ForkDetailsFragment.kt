package fr.loutry.github.ui.fork.details

import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import fr.loutry.github.R
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.ui.fork.details.models.ForkMapper
import fr.loutry.github.ui.fork.details.views.ForkDetailsWidget

class ForkDetailsFragment : Fragment() {

    companion object {
        private const val DEFAULT_ID_NOT_FOUND = "id not found"
    }

    private lateinit var viewModel: ForkDetailsViewModel

    private lateinit var forkDetailsWidget: ForkDetailsWidget

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fork_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        forkDetailsWidget = view.findViewById(R.id.fork_details_widget)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        context?.let { context ->
            initViewModel(context)
        }
    }

    private fun initViewModel(context: Context) {
        val dateFormat = DateFormat.getDateFormat(context)
        val factory = ForkDetailViewModelFactory(
            forkId = getForkId(),
            database = ForksDatabase.getInstance(context),
            forkMapper = ForkMapper(dateFormat)
        )

        viewModel = ViewModelProviders.of(this, factory)
            .get(ForkDetailsViewModel::class.java)

        viewModel.uiData.observe(this, Observer { uiState ->
            forkDetailsWidget.handleState(uiState)
        })
    }

    private fun getForkId(): String {
        return arguments?.getString("forkId")?: DEFAULT_ID_NOT_FOUND
    }
}