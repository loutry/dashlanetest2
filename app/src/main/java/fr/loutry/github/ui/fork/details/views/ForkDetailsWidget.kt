package fr.loutry.github.ui.fork.details.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ScrollView
import fr.loutry.github.R
import fr.loutry.github.ui.fork.details.models.ForkDetailUiState

class ForkDetailsWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ScrollView(context, attrs, defStyleAttr) {

    private val blankStateView: BlankView
    private val errorStateView: ErrorView
    private val idealStateView: ForkDetailsView

    init {
        View.inflate(context, R.layout.fork_details_widget, this)
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )

        blankStateView = findViewById(R.id.fork_details_blank_state)
        errorStateView = findViewById(R.id.fork_details_error_state)
        idealStateView = findViewById(R.id.fork_details_ideal_state)
    }

    fun handleState(uiState: ForkDetailUiState?) {
        hideAllStates()
        when (uiState) {
            ForkDetailUiState.Blank -> blankStateView.visibility = View.VISIBLE
            ForkDetailUiState.Error -> errorStateView.visibility = View.VISIBLE
            is ForkDetailUiState.Ideal -> {
                setIdealStateData(uiState)
                idealStateView.visibility = View.VISIBLE
            }
        }
    }

    private fun hideAllStates() {
        blankStateView.visibility = View.GONE
        errorStateView.visibility = View.GONE
        idealStateView.visibility = View.GONE
    }

    private fun setIdealStateData(uiState: ForkDetailUiState.Ideal) {
        idealStateView.setImage(uiState.avatarUrl, uiState.avatarContentDescription)
        idealStateView.setTitle(uiState.title)
        idealStateView.setDescription(uiState.description)
        idealStateView.setCreationDate(uiState.creationDate)
    }

}
