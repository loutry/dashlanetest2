package fr.loutry.github.ui.fork.details.models

sealed class ForkDetailUiState {

    object Blank : ForkDetailUiState()

    object Error : ForkDetailUiState()

    data class Ideal(
        val avatarUrl: String,
        val avatarContentDescription: String,
        val title: String,
        val description: String,
        val creationDate: String
    ) : ForkDetailUiState()
}