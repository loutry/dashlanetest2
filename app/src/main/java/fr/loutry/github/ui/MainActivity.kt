package fr.loutry.github.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import fr.loutry.github.R
import fr.loutry.github.database.ForksDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.forks_menu_clear_data -> {
                clearData()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun clearData() {
        val database = ForksDatabase.getInstance(this)
        CoroutineScope(Dispatchers.Default).launch {
            database.clearAllTables()
        }
    }
}
