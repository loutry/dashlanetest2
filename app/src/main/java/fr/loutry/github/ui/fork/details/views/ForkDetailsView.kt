package fr.loutry.github.ui.fork.details.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import fr.loutry.github.R

class ForkDetailsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val imageView : ImageView
    private val titleView : TextView
    private val descriptionView : TextView
    private val creationDateView : TextView

    init {
        View.inflate(context, R.layout.fork_details_layout, this)
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )

        imageView = findViewById(R.id.fork_details_image)
        titleView = findViewById(R.id.fork_details_title)
        descriptionView = findViewById(R.id.fork_details_description)
        creationDateView = findViewById(R.id.fork_details_creation_date)
    }

    fun setImage(imageUrl: String, contentDescription: String) {
        imageView.contentDescription = contentDescription
        Picasso.get()
            .load(imageUrl)
            .fit()
            .centerCrop()
            .placeholder(R.drawable.shape_forks_avatar_placeholder)
            .into(imageView)
    }

    fun setTitle(title: String) {
        titleView.text = title
    }

    fun setDescription(description: String) {
        descriptionView.text = description
    }

    fun setCreationDate(creationDate: String) {
        creationDateView.text = creationDate
    }
}
