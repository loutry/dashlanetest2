package fr.loutry.github.ui.fork.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.ui.fork.details.models.ForkMapper

class ForkDetailViewModelFactory(
    private val forkId: String,
    private val database: ForksDatabase,
    private val forkMapper: ForkMapper
    ) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ForkDetailsViewModel(forkId, database, forkMapper) as T
    }
}
