package fr.loutry.github.ui.fork.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.loutry.github.database.ForksDatabase
import fr.loutry.github.database.models.ForkDbModel
import fr.loutry.github.ui.fork.details.models.ForkDetailUiState
import fr.loutry.github.ui.fork.details.models.ForkMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ForkDetailsViewModel(
    forkId: String,
    database: ForksDatabase,
    forkMapper: ForkMapper
) : ViewModel() {

    private val _uiData = MutableLiveData<ForkDetailUiState>()
    val uiData: LiveData<ForkDetailUiState>
        get() = _uiData

    init {
        _uiData.postValue(ForkDetailUiState.Blank)
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val fork = database.getDao().getFork(forkId)
                postUiState(fork, forkMapper)
            }
        }
    }

    private fun postUiState(
        fork: ForkDbModel?,
        forkMapper: ForkMapper
    ) {
        when (fork) {
            null -> _uiData.postValue(ForkDetailUiState.Error)
            else -> {
                val idealUiState = forkMapper(fork)
                _uiData.postValue(idealUiState)
            }
        }
    }
}
