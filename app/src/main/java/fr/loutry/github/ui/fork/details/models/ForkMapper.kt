package fr.loutry.github.ui.fork.details.models

import android.annotation.SuppressLint
import fr.loutry.github.database.models.ForkDbModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ForkMapper(private val dateFormat: DateFormat) {

    @SuppressLint("SimpleDateFormat")
    private val simpleDateFormat = SimpleDateFormat(GITHUB_DATE_PATTERN)
        .apply { timeZone = TimeZone.getTimeZone(UTC) }


        operator fun invoke(forkDbModel: ForkDbModel): ForkDetailUiState.Ideal {
            val creationDate = simpleDateFormat.parse(forkDbModel.creationDate)
            return ForkDetailUiState.Ideal(
                avatarUrl = forkDbModel.userAvatar,
                avatarContentDescription = forkDbModel.userLogin,
                title = forkDbModel.title,
                description = forkDbModel.description,
                creationDate = dateFormat.format(creationDate)
        )
    }

    companion object {
        private const val GITHUB_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        private const val UTC = "UTC"
    }
}