package fr.loutry.github.ui.fork.master

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.loutry.github.api.GitHubService
import fr.loutry.github.database.ForksDatabase

class ForksViewModelFactory(
    private val api: GitHubService,
    private val database: ForksDatabase
    ) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ForksViewModel(api, database) as T
    }
}
