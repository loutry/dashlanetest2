package fr.loutry.github.api

object ApiDefault {
    const val FIRST_PAGE = 1
    const val PAGE_SIZE = 100
    const val OWNER = "DefinitelyTyped"
    const val REPOSITORY = "DefinitelyTyped"
}
