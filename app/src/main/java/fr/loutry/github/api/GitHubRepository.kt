package fr.loutry.github.api

import android.content.res.Resources
import fr.loutry.github.R
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object GitHubRepository {
    fun getApi(resources: Resources): GitHubService {
        val retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.base_url))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        return retrofit.create(GitHubService::class.java)
    }
}
