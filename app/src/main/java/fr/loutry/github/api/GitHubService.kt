package fr.loutry.github.api

import fr.loutry.github.api.models.ForkApiModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubService {

    @GET("repos/{owner}/{repository}/forks")
    suspend fun listForks(
        @Path("owner") owner: String,
        @Path("repository") repository: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): List<ForkApiModel>
}
