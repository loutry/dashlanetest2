package fr.loutry.github.api.models

data class LicenseApiModel(
    val key: String,
    val name: String,
    val node_id: String,
    val spdx_id: String,
    val url: Any
)
